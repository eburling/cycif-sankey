# Building cell state sankeys using holoviews

The dataframe `sk_df` is cell-by-marker and describes the state of each cancer cell from a breast cancer TMA as measured by cycIF and defined by manual intensity thresholding. Note that `sk_df` contains data for cancer cells only and is sourced from a master dataframe `thresh` which contained the states of all cells in the TMA. Cancer cells were defined using the following scheme:

```
sk_df = thresh[
    (
    (thresh['CK19_Ring']==True) | 
    (thresh['CK14_Ring']==True) | 
    (thresh['CK7_Ring']==True) | 
    (thresh['CK5_Ring']==True) |
    (thresh['Ecad_Ring']==True) |
    (thresh['ER_Nuclei']==True)) & 
    (thresh['CD45_Ring']==False) &
    (thresh['CD31_Ring']==False) &
    (thresh['CD68_Ring']==False)
    )
]
```

The sankey affords a hierarchical view of cancer cell state and composition, where the granularity of the view increases moving from left to right in the figure.

<p align="left"><img width="99%" src="assets/sankey.gif" /></p>
